
resource "linode_stackscript" "stackscript" {
  for_each = { for stsc in var.stackscripts : stsc.name => stsc }
	
  label = each.key
  description = each.value.description
  script = fileexists(each.value.file) ? file("${each.value.file}") : file("${path.module}/${each.value.file}")
  images = each.value.images
  rev_note = each.value.version


}
