#!/bin/bash
# <UDF name="hostname" label="Hostname" example="ceph1" default="">
# <UDF name="domain" label="DNS network domain" example="example.org" default="">
# <UDF name="myuser" label="Default admin user" example="ceph1" default="">
# <UDF name="adminpass" label="Default admin password" example="S3cr3t" default="Scr3W7h4t,y0UPir47z">
# <UDF name="pubkey" label="Default admin pubkey" example="" default="">

source <ssinclude StackScriptID="1">

system_set_hostname $HOSTNAME.$DOMAIN

system_install_package nginx

# Create MYUSER
user_add_sudo $MYUSER $ADMINPASS
user_add_pubkey "${MYUSER}" "${PUBKEY}"

# Update Distribution
system_update

touch stackscript_done