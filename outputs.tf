output "nodes_addresses" {
  description = "Nodes addresses"
  value = { for k,v in linode_instance.node: k => {
		"ipv4": join(" ",v.ipv4),
		"ipv6": regex("[a-f:0-9]+",v.ipv6),
		"ip_privee": v.private_ip_address,
		"ip_public": v.ip_address,
		}
  }
}
