
variable "authorized_keys" {
	type = list
}
variable "nodes" {
	description = "List of nodes to create"
	type = list(object({
	  name = string
	  region = optional(string,"us-central")
	  type = optional(string,"g6-nanode-1")
	  image = optional(string,"linode/ubuntu22.04")
	  disk_label = optional(string,"srv")
	  disk_size = optional(string,"20")
	  swap_size = optional(string,"512")
	  stackscript = optional(string,"base")
	  stackscript_datas = optional(map(string),
	  {
		"domain"="example.local",
		"myuser"="webmaster",
		"adminpass"="",
		"pubkey"="ssh-rsa AAAAB3Nz....0ffdAp5 my_home_key",
	  })
	}))
}

variable "stackscripts" {
	description = "List of nodes to create"
	type = list(object({
	  name = string
	  description = string
	  images = optional(list(string),["any/all"])
	  version = optional(string,"v0.1")
	  file = string
	}))
	default = [
	  {
	    name = "base"
	    description = "Hostname, User & Update"
	    file = "stackscripts/base.sh"
	  }
	]
	nullable = false
}
