
terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      #version = "1.30.0"
    }
  }
}

resource "linode_instance" "node" {
  for_each = { for node in var.nodes : node.name => node }

  label            = each.key
##
  authorized_keys  = var.authorized_keys
  
  region           = each.value.region
  type             = each.value.type

  private_ip = true
  boot_config_label   = "boot_config"

}

resource "linode_volume" "sdc" {
  for_each = { for node in var.nodes : node.name => node }
  label = "${each.key}-${each.value.disk_label}"
  region = linode_instance.node[each.key].region
  size = each.value.disk_size
}

# Create a swap disk
resource "linode_instance_disk" "swap" {
  for_each = { for node in var.nodes : node.name => node }
  label = "swap"
  linode_id = linode_instance.node[each.key].id
  size = each.value.swap_size
  filesystem = "swap"
}

# Create a boot disk
resource "linode_instance_disk" "boot" {
  for_each = { for node in var.nodes : node.name => node }
  
  label = "boot"
  linode_id = linode_instance.node[each.key].id
  filesystem = "ext4"
  size = linode_instance.node[each.key].specs.0.disk - linode_instance_disk.swap[each.key].size

  image            = each.value.image
  authorized_keys  = var.authorized_keys

  stackscript_id   = linode_stackscript.stackscript[each.value.stackscript].id
  stackscript_data = merge({"hostname"=each.key},each.value.stackscript_datas)
		
}




resource "linode_instance_config" "config" {
  for_each = { for node in var.nodes : node.name => node }

  linode_id = linode_instance.node[each.key].id
  label = "boot_config"

  device {
    device_name = "sda"
    disk_id = linode_instance_disk.boot[each.key].id
  }
  device {
    device_name = "sdb"
    disk_id = linode_instance_disk.swap[each.key].id
  }
  device {
    device_name = "sdc"
    volume_id = linode_volume.sdc[each.key].id
  }
  interface {
    #purpose = "vlan"
    purpose = "public"
  }
  

  root_device   = "/dev/sda"
  kernel        = "linode/grub2"
  run_level     = "default"
  virt_mode     = "paravirt"
  booted = true

  helpers {
          devtmpfs_automount = true
          distro             = true
          modules_dep        = true
          network            = true
          updatedb_disabled  = true
      }
}
