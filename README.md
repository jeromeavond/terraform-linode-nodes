# Terraform Linode nodes creator Module

## TODO

- [X] ~~Set hostname in stackscript-datas to be always node name (no need to put it in variable default)~~
- [ ] Create default stackscript, then merge default + scripts from variable
- [ ] All in env variables
  - [ ] Set all in environment variable => yamlfile variable contains nodes/stackscripts/authkey
  - [ ] MAYBE : Get the ```terraform init``` line from gitlab and copy paste as variable also

## Requirements

- A Akamaï/Linode account, get an API_TOKEN
- A gitlab/github account, get an API_TOKEN
  - not sure if working with bitbucket
  - this is for saving terraform states, might you give test 

## Example use

### If you need stackscripts

- Create directory ```scripts```
- Put some [StackScripts](https://www.linode.com/docs/products/tools/stackscripts/) files in it

example: ```scripts/base.sh```

```shell
#!/bin/bash
# <UDF name="hostname" label="Hostname" example="node1" default="node1">
# <UDF name="myuser" label="Default user" example="webmaster" default="webmaster">
# <UDF name="domain" label="Server DNS network domain" example="example.org" default="example.local">
# <UDF name="adminpass" label="Admin User Password" example="S3cr3t" default="Screw7h4t,Y0Up1r4tz">
# <UDF name="pubkey" label="Default admin pubkey" example="" default="">

source <ssinclude StackScriptID="1">

system_set_hostname $HOSTNAME.$DOMAIN

system_install_package nginx

# Create MYUSER
user_add_sudo $MYUSER $ADMINPASS
user_add_pubkey "${MYUSER}" "${PUBKEY}"

# Update Distribution
system_update

touch stackscript_done
```

### Basic deployement main.tf

You can copy/paste that definition:

```t
# Terraform configuration

terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }
  }
}

provider "linode" {
  # Configuration options
  # token = Set your LINODE_TOKEN env var
}

# Module

module "linode_module" {
  
  source = "git::https://framagit.org/jeromeavond/terraform-linode-nodes/"
  authorized_keys = var.authkeys
  nodes = var.nodes
  stackscripts = var.stackscripts

# Variables definitions

variable "authkeys" {
	type = list
}

variable "nodes" {
	description = "list of nodes"
	type = list
}
variable "stackscripts" {
	description = "list of stackscripts"
	type = list
}

# Outputs

output "nodes-addresses" {
	value = module.linode_module.nodes_addresses
}

}
```

### Variables

> Note:
> 
> variable defined : REQUIRED
>
> #variable commented : OPTIONAL

```yaml
nodes:
  - name: node1
    # region: us-central
    # type: g6-nanode-1
    # image: linode/ubuntu22.04
    # disk_label: srv
    # disk_size: 20
    # swap_size: 512
    # stackscript: base
    # stackscript_datas:   # dépend du script
    #   !!NEVER SET!! hostname: mynode (Now autoset to object name)
    #   myuser: jerome
    #   fqdn: mynode.toto
    #   domain: toto
    #   adminpass: adminpass.toto
    #   hosts: 
    #   ipprv: 
    #   ippub: 
  - name: node2
    stackscript: nginxphp
    stackscript_datas:
      hostname: web
      domain: example.net
      myuser: webmaster

stackscripts:
  - name: docker
    description: Docker basic install
    file: scripts/docker.sh
    # version: v0.1
    # images:
    #  - any/all
  - name: nginxphp
    description: Basic Nginx/Php install
    file: scripts/nginxphp.sh
    images:
      - linode/debian10
      - linode/debian11
      - linode/debian12
      - linode/ubuntu22.04

authkeys:
  - ssh-rsa AAAAB3Nz....0ffdAp5 my_home_key
  - ssh-rsa AAAAB3Nz....IfjPUMX my_work_key

```

- Put this in a .yaml file
- Convert to .auto.tfvars.json file
- Init, plan and apply

```shell
$ yq . nodes.yaml -o json > nodes.auto.tfvars.json
$ terraform init
$ terraform plan
$ terraform apply
```
